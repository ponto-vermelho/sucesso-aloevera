<?php echo ("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>"); ?>

<!DOCTYPE html public "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt" lang="pt">
	<head>
		<title>SUCESSO ALOE VERA</title>
		<link rel="stylesheet" href="/style/screen.css" type="text/css" media="screen" />
		<script language="javascript" src="/style/commons.js"></script>
	</head>

	<body>
		<div id="header"><!-- INICIO DE #HEADER -->
			<h1>Sucesso Aloe Vera<span>&nbsp;</span></h1>
			<div id="menu"><!-- INICIO DE #MENU -->
				<ul>
					<li><a href="inicio" title="Home (1)" accesskey="1">home</a></li>
					<li><a href="contato" title="Contato (2)" accesskey="2">contato</a></li>
					<li><a href="produtos" title="Nossos produtos (3)" accesskey="3">nossos produtos</a></li>
					<li><a href="sobre" title="Sobre aloe vera (4)" accesskey="4">sobre aloe vera</a></li>
					<li><a href="formas" title="Formas de ganho (5)" accesskey="5">formas de ganho</a></li>
				</ul>
			</div><!-- FIM DE #MENU -->
			<div id="informacoes"><!-- INICIO DE #INFORMACOES -->
				<div class="vcard"><!-- INICIO DE .VCARD -->
					<p><img src="/img/leonardo.jpg" alt="Leonardo Costa" class="photo"/></p>
					<h2 class="fn">Leonardo Costa</h2>
					<p class="org">Empresário</p>

					<p class="tel"><span class="type">Comercial</span>: <span class="value">22 2725-4109</span></p>
					<p class="tel"><span class="type">Celular</span>: <span class="value">22 9917-0073</span></p>

					<p class="adr"><span class="locality">Campos</span> - <span class="region">RJ</span></p>
					<p><a class="email" href="/contato" title="Contato com o Leonardo Costa">contato</a></p>
				</div><!-- FIM DE .VCARD -->
				<div class="banner"><!-- INICIO DE #BANNER -->
					<h2>Frases de sucesso</h2>
					<ul>
						<li>"Lucros são melhores que salários"</li>
						<li>"Pessoas buscam liberdade"</li>
						<li>"Decida a velocidade do seu sucesso"</li>
						<li>"Torne-se um empresário"</li>
					</ul>
				</div><!-- FIM DE #BANNER -->
			</div><!-- FIM DE #INFORMACOES -->
			<div class="bambooh">&nbsp;</div>
		</div><!-- FIM DE #HEADER -->
