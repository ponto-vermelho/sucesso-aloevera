		<div id="conteudounico"><!-- INICIO DE #CONTEUDO -->
			<div class="top">&nbsp;</div>
			<div id="unico"><!-- INICIO DE #UNICO -->

<h1>Nossos Produtos</h1>
<p>A conhecida <strong>aloe vera</strong> ou <strong>babosa</strong> é uma planta pertencente a família das liláceas, da qual também faz parte o alho. ambos possuem propriedades curativas e nutritivas.</p>
<h2>Sucos de Aloe Vera</h2>
<p><img src="/img/suco.jpg" alt="Experimente nossos sucos" class="produto" width="110px" /></p>
<p> Para cada ação, um produto específico.</p>
<ul>
	<li>Concentrados de Nutrientes</li>
	<li>Ajuda a manter a saúde do sistema urinário</li>
	<li>Reduz os efetios da TPM</li>
	<li>Manutenção da saúde das articulações</li>
</ul>
<h2>Suplementos Nutricionais</h2>
<p><img src="/img/suplemento.jpg" alt="Tenha mais vitalidade" class="produto" width="170px"  /></p>
<ul>
	<li>Pó nutricional (chocolate/baunilha)</li>
	<li>Vitamina C</li>
	<li>Multivitaminico</li>
	<li>Alho com tomilho</li>
	<li>Fibras</li>
	<li>Ômega 3</li>
	<li>Cálcio</li>
	<li>Própolis</li>
	<li>Polém</li>
	<li>Geléia Real</li>
</ul>
<h2>Proteção para pele</h2>
<p><img src="/img/creme.jpg" alt="Proteja-se!" class="produto" width="120px"  /></p>
<ul>
	<li>Creme de Massagem</li>
	<li>Creme com Própolis</li>
	<li>Protetor Solar</li>
	<li>Bronzeador Solar</li>
	<li>Hidratante Pele Seca</li>
	<li>Loção Regeneradora</li>
	<li>Protetor Labial</li>
	<li>Hidratante Corporal c/ colágeno e elastina</li>
	<li>Creme para contorno dos olhos</li>
	<li>Gel Cicatrizante</li>
</ul>
<h2>Produtos de Beleza</h2>
<p><img src="/img/pele.jpg" alt="Nossos produtos de beleza" class="produto" width="100px"  /></p>
<ul>
	<li>Kit Redução de Medidas</li>
	<li>Esfoliante Facial</li>
	<li>Removedor de Maquiagem</li>
	<li>Tônico</li>
	<li>Anti-rugas</li>
	<li>Creme Noturno</li>
	<li>Perfume</li>
</ul>
<h2>Cuidados Pessoais</h2>
<p><img src="/img/banho.jpg" alt="Tenha cuidado com você mesmo" class="produto" width="160px"  /></p>
<ul>
	<li>Condicionador</li>
	<li>Shampoo</li>
	<li>Sabonete L íquido</li>
	<li>Loção P&oacute;s Barba </li>
	<li>Desodorante</li>
	<li>Creme Dental</li>
	<li>Fixador de Cabelo</li>
</ul>
<p>Todos os nossos produtos s&atilde;o naturais a base de Aloe Vera (Babosa).</p>

			
			</div><!-- FIM DE #UNICO -->
			<div class="bottom">&nbsp;</div>
		</div><!-- FIM DE #CONTEUDO -->