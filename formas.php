		<div id="conteudounico"><!-- INICIO DE #CONTEUDO -->
			<div class="top">&nbsp;</div>
			<div id="unico"><!-- INICIO DE #UNICO -->

<h1>Seja bem vindo ao início do seu sucesso e de sua independência financeira. Eu sou um Empresário e você também será um!</h1>
<p>Com esse sistema fantástico de distribuição interativa você poderá ser um consumidor, um distribuidor e poderá construir sua renda. Três formas de sucesso!</p>
<h3> Efetuar seu cadastro na empresa e consumir com descontos</h3>
<p>Ao efetuar seu cadastro na empresa você poderá adiquirir produtos sem intermediários com até 30% de desconto. Ao se cadastrar, você não é obrigado a comprar. Muitos dos nossos empresários independentes foram antigos consumidores e é onde você irá chegar.</p>
<h3>Distribuidor dos produtos</h3>
<p> Se torne um empresário independente e compre produtos a preço de atacado, revenda-os com lucros de até 43%, como por exemplo, compre 100 reais em produtos e com a venda ganhe 43 reais e ainda ganhe bônus (caixas de créditos).</p>
<h3>Construir renda (marketing de rede)</h3>
<p> Este sim é o passo mais importante e que nos traz mais rentabilidade. É muito simples e fácil, quando usamos os produtos e os indicamos, você, como empresário independente recebe por isso, e cada pessoa que se cadastra como empresário independente a partir de você, ou quando adquire produtos ou quando recebe bônus.</p>
<p>Saiba como se tornar um distribuidor ou um assistente de supervisor fazendo seu cadastro aqui no site ou participando de uma de nossas reuniões - É muito simples e vale a pena!</p>
<p class="destaque">Você pode no período de até 6 (seis) meses se tornar um gerente e ganhar em torno de 5.604 reais, ou muito mais, <span style="color: #006600">a sua velocidade vai gerar seus resultados.</span></p>
<p>Em nosso sistema de marketing de rede você se cadastra e adquire 01 cc (caixa crédito), chegando a gerente com 120 cc, como segue o sistema:</p>
<p style="text-align: center;">
	<img src="/img/caixascredito.jpg" alt="Esquema das caixas crédito"  />
</p>
<p>Importante lembrar que todas as pessoas que se cadastram ou adquirem produtos, te acrescentam caixas créditos. quando você completa as 120 cc você se torna um gerente, a cada nível conquistado os valores aumentam, e seu cheque mensal só engorda.</p>
<p>Cadastrado em nosso site, você receberá informações detalhadas de como conseguir dinheiro, saúde e sucesso!</p>
			
			</div><!-- FIM DE #UNICO -->
			<div class="bottom">&nbsp;</div>
		</div><!-- FIM DE #CONTEUDO -->