		<div id="conteudounico"><!-- INICIO DE #CONTEUDO -->
			<div class="top">&nbsp;</div>
			<div id="unico"><!-- INICIO DE #UNICO -->

<h1>Sobre Aloe Vera</h1>
<p>O nome <strong>aloe vera</strong> seria originário do hebráico <em>halal</em> ou do arábico <em>alloeh</em> (substância amarga, brilhante) e do latim <em>vera</em> (verdadeira). Ao que tudo indica, ela é considerada uma planta poderosa há muito tempo. Antigos muçulmanos e judeus acreditavam que a babosa representava uma proteção para todos os males e, por isso, usavam as folhas até penduradas na porta de entrada da casa. Alexandre, o Grande, teria conquistado as Ilhas de Socotorá, no Oceano Índico (século IV a.C.), porque lá vegetava abundantemente um tipo de babosa que produzia uma tinta violácea. Há quem diga, entretanto, que na verdade, o conquistador conhecia os poderes cicatrizantes da babosa e seu principal interesse nas ilhas era ter plantas suficientes para curar os ferimentos dos seus soldados após as batalhas.</p>
<p>Quanto às suas virtudes cosméticas, basta dizer que ao serem descobertos os segredos de beleza de Cleópatra, a polpa da babosa ocupava lugar de destaque. O que parece ser confirmado pela indústria atual, pois o ingrediente entra na fabricação de inúmeros cremes, loções bronzeadoras, shampoos, condicionadores, máscaras, etc. Existem centenas de espécies de babosa, a maioria de origem africana, pertencentes à Família das Lileáceas. As espécies mais conhecidas como medicinais são a Aloe vera, que nasce em forma de tufo e produz flores amarelas e a Aloe arborescens, que nasce em torno de um pequeno tronco e produz flores alaranjadas e vermelhas.</p>
<p>Fonte: <a href="http://www.jardimdeflores.com.br/ervas/babosa1.html" title="Site: www.jardimdeflores.com.br">www.jardimdeflores.com.br</a></p>
<h2>O que a Aloe Vera pode fazer por você</h2>
<p>A <strong>aloe vera</strong> tem quatro propriedades principais: anti-inflamatório, fungicida, antibiótico e regenerador.</p>
<p>Esta planta se distingue dos outros remédios naturais por suas propriedades únicas que agem individualmente sobre alvos específicos no organismo, mesmo se eles agissem também sinergéticamente. A <strong>aloe vera</strong> possui um potencial enorme como medicamento e  pode facilmente substituir os esteróides de uso tópico e orais e os antibióticos de uso tópico. A planta possui forte teor de enzimas (92 enzimas), o que a torna na realidade um recurso raro e precioso porque as enzimas ajudam a assimilar melhor os nutrientes essenciais e a purificar o organismo. Esta planta, ainda, reduz os efeitos secundários dos medicamentos e reforça o sistema imunitário, além de ajudar o nosso sistema a eliminar as toxinas acumuladas.</p>
<p>A <strong>aloe vera</strong> tem um poder de penetração através da pele quatro vezes mais rápido que a água. Esta poderosa planta não perturba a fisiologia, ele a respeita, a equilibra e melhora-a porque, além de ter grande espectro de propriedades nutritivas e curativas, ela é rica em vitaminas, sais minerais, ácidos aminados e enzimas. A <strong>aloe vera</strong> conta com elementos ativos que atuam da cabeça aos pés, dependendo das necessidades de nosso corpo. Ela regenera os tecidos, respeitando o ritmo normal do trabalho das células.</p>
<p class="destaque">Aloe Vera é um agente de limpeza natural que penetra nos tecidos do corpo mais rapidamente.</p>
<ul>
	<li>Anestesia os tecidos do corpo com os quais entra em contato, em uma atuação em profundidade.</li> 
	<li>Tem função bactericida, anti-virus e fungicida.</li>
	<li>Reduz o tempo de cura.</li>
	<li>Diminui a febre e a elevação localizada de temperatura.</li>
	<li>É um anti-inflamatório muito poderoso.</li>
	<li>Elimina coceiras e comichões.</li> 
	<li>É nutritivo (contém uma grande variedade de vitaminas, sais minerais e glucídios).</li> 
	<li>Ajuda a multiplicação celular.</li> 
</ul> 
<p class="destaque">Totalmente inofensivo. Nenhum efeito tóxico/luzerna.</p>
<p>Fonte: <a href="http://www.santrel.com/poindexa.htm" title="Site: http://www.santrel.com">www.santrel.com</a></p>


			
			</div><!-- FIM DE #UNICO -->
			<div class="bottom">&nbsp;</div>