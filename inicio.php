		<div id="conteudo"><!-- INICIO DE #CONTEUDO -->
			<div id="principal"><!-- INICIO DE #PRINCIPAL -->
				<div class="top">&nbsp;</div>
				<h1>"Saúde e bem estar" Aloe vera (babosa)</h1>
				<h2>Produtos para uma vida saudável</h2>
				<p><img class="center" src="/img/selos.gif" alt="Selos dados a nossa empresa"  /></p>
				<p class="icone_aloevera">A <em>aloe vera</em> (babosa) é uma planta conhecida por possuir grandes propriedades curativas e nutritivas, dentre ela mencionamos as seguintes:</p>
				<ul>
					<li>Função inibidora da dor</li>
					<li>Ação anti-inflamatória</li>
					<li>Ação coagulante</li>
					<li>Ação queratolítica</li>
					<li>Ação antibiótica</li>
					<li>Ação regenadora celular (câncer de pele)</li>
					<li>Ação energizante</li>
					<li>Ação digestiva</li>
					<li>Ação desintoxicante</li>
					<li>Ação rehidratante da pele</li>
					<li>Ação nutritiva</li>
					<li>Ação transportadora</li>
					<li>Aloe vera na odontologia</li>
					<li>A aloe vera no tratamento de beleza</li>
				</ul>
					<p class="destaque"><span class="center">Nossa empresa é a maior produtora e distribuidora de <strong>aloe vera</strong> do mundo!!!</span></p>
				<p><img class="center" src="/img/plantacao.jpg" alt="Nossa empresa é a maior produtora e distribuidora de aloe vera do mundo!!!" /></p>
				<div class="bottom">&nbsp;</div>
			</div><!-- FIM DE #PRINCIPAL -->
			<div class="secundario"><!-- INICIO DE #SECUNDARIO -->
				<div class="top">&nbsp;</div>
				<h1>Torne-se um empresário!</h1>
				<div id="video">
					<p><img src="/img/video.jpg" alt="Assista o nosso video"  /></p>
				</div>
				<h2 class="center">"O próximo trilhão"</h2>
				<p>Trabalhe a partir de casa em seu tempo livre, agregue valores a sua vida e ao seu bolso. Cuide de sua saúde, de sua família e de seus amigos e ainda ganhe dinheiro com este negócio. Não é venda de produto algum, é marketing de rede.</p>
				<p>Não faça propaganda de um produto que está em promoção ou de um outro que você gostou, porque você não ganha nada com isso. Mas nós pagamos você.</p>
				<h3>É simples assim:</h3>
				<ul>
					<li>Use,  aprove e compartilhe</li>
					<li>Fale com pessoas sobre o produto e seus benfícios</li>
					<li>Participe dos eventos e palestras</li>
					<li>Você precisa ter no mínimo 4 amigos</li>
				</ul>
				<div class="bottom">&nbsp;</div>
			</div>

		</div><!-- FIM DE #CONTEUDO -->


		<div id="conteudounico"><!-- INICIO DE #CONTEUDO -->
			<div class="top">&nbsp;</div>
				<div id="unico"><!-- INICIO DE #UNICO -->
					<h2>"Não é venda. É marketing." Este sistema mudará a sua vida para sempre.</h2>
					<h2 style="text-align: center;">Uma revolução mercadológica!!! Uma tendência do século 21!!!</h2>
					<p>"Dos 500 mil milionários americanos, 100 mil construiram renda com este negócio." São pessoas comuns e com as mesmas necessidades que nós. Elas acreditaram. <a href="formas" title="Saiba mais sobre a aloe vera!">Saiba mais</a></p>
				</div>
			<div class="bottom">&nbsp;</div>
